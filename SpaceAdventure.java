import java.util.;
import java.io.File;
import java.util.Scanner;
/**
 * Space Adventure Assignment, review of grade 11 concepts
 */
public class SpaceAdventure 
{ 
  PlanetarySystem planetarySystem;
  
  //The constructor method, creates the 8 planets in the solar system
  /**
   * Q2. First create a new planet and input a string name and string description, and then assign that planet to an index in the array
   * Q3. We can repeat the above step and assign different index values of the array to each planet
   * Q4. If there are more than 8 planets we could use a loop 
   * Q5. To refactor the code means to change the programs internal stucture without changing its functional behavior
   */
  public SpaceAdventure()
  { 
    File file = new File("PawanSolarSystem.txt");
    Scanner scan = new Scanner(file);
    String line = scan.nextLine();
      /*
      *Planet [] planets= new Planet[8];
      *Planet mercury = new Planet("Mercury", "A very hot  planet, closest to the sun.");
      *planets[0] = mercury;
      *Planet neptune = new Planet("Neptune", "A very cold  planet, furthest from the sun.");
      *planets[1] = neptune;
      *Planet venus = new Planet("Venus", "The second planet from the Sun, it has no natural satellites.");
      *planets[2] = venus;
      *Planet earth = new Planet("Earth", "The only planet known to harbor life.");
      *planets[3] = earth;
      *Planet mars = new Planet("Mars", "This planet is known as the 'red' planet.");
      *planets[4] = mars;
      *Planet jupiter = new Planet("Jupiter", "This planet is the largest in the solar system.");
      *planets[5] = jupiter;
      *Planet saturn = new Planet("Saturn", "This planet has beautiful rings around it.");  
      *planets[6] = saturn;
      *Planet uranus = new Planet("Uranus", "This planet orbits on its side, with its axis titled almost perpendicular to the Sun.");
      *planets[7] = uranus;
      *planetarySystem = new PlanetarySystem("Solar System", planets);
  }
    
  // Runs the program, creates a new SpaceAdventure class 
  public static void main(String[] args) 
  {
      SpaceAdventure adventure = new SpaceAdventure();
      adventure.start();
  }
  
  //Runs the introduction
  /**
   * Q9. There is a runtime error because the randomizer goes from 0-8, however the array only has an index of 7. Therefore when the random int=8, there will be an error as there is no 8th index. We can fix this my changing randomInt to be in between index 0-7
   */
  private void start()
  {
      displayIntroduction();
      greetAdventurer();
      if(!(planetarySystem.planets==null || planetarySystem.planets.length==0))
      {
          System.out.println("Let's go on an adventure!");
          determineDestination();
      }
  }
  
  //Method includes what is in the introduction
  /**
   * Q1. the number of planets should come from the planetary system class
   */
  private void displayIntroduction()
  {
      System.out.println("Welcome to the " + planetarySystem.name + "!");
      System.out.println("There are " + planetarySystem.planets.length + " planets to explore.");
      System.out.println("You are currently on Earth, which has a circumference of 24859.82 miles.");
  }
  
  //To get user input
  private static String responseToPrompt(String prompt)
  {
      System.out.println(prompt);
      Scanner input=new Scanner(System.in);
      return input.nextLine();
  }
  
  //To greet the adventure and ask for a name
  private void greetAdventurer() 
  {
      String name = responseToPrompt("What is your name?");
      System.out.println("Nice to meet you, " + name + ". My name is Eliza.");
    }
    
  // To determine which planet the user wants to visit
  /**
   * Q6. Ask for user input to see if the user would like the program to choose a planet to visit or not, if the user inputs "N" then ask a prompt to specify the planet the user wishes to visit
   * Q7. Use the responseToPrompt method to get user input for the name of the planet they would like to visit, compare the user input to the names in the array and then print out that planets name and description if it exists
   */
  private void determineDestination() 
  {
      String decision = "";
      while (!(decision.equals("Y") || decision.equals("N")))
      {
          decision = responseToPrompt("Shall I randomly choose a planet for you to visit? (Y or N)  ");
          if (decision.equals("Y"))
          {
              Planet planet=planetarySystem.randomPlanet();
              if(planet!=null)
              {
                  visit(planet.name);
              }
              else
              {
                  System.out.println("Sorry, but there are no planets in this system.");
              }
          }
          else if (decision.equals("N") )
          {
              String planetName= responseToPrompt("Ok, name the planet you would like to visit.");
              visit(planetName);
          }
          else 
          {
              System.out.println("Huh? Sorry, I didn't get that.");
          }
      }
  }
  
  //This method specifies where the user is going, it prints out the name and description of the planet if the user inputs a planet that matches one in the array
  /**
   * Q8. If an unknown planet name is entered as user input then only "Traveling to + planetName" will be the output
   */
  private void visit(String planetName) 
  {
      System.out.println("Traveling to " + planetName + "...");
      for(Planet planet: planetarySystem.planets)
      {
          if(planetName.equals(planet.name))
          {
              System.out.println("Arrived at " + planet.name + ". " + planet.description);
          }
      }    
  }
  
  //Q10. To improve this code, we could try to use more loops and minimize the repition of code
}