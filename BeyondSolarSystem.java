import java.util.Scanner;
import java.util.Random;
/**
 * SpaceAdventure Game. 
 *
 * @author Pawan Iyer
 * @version Feb 14, 2018
 * 
 * Answers:
 * 1 - Instead of an independent numberOfPlanets variable, where do you think the number of planets should come from? 
 * The number of planets shouls come from the length of the array, instead of an independent variable.
 * 
 * 2 - We have an array, but how do we add planets to the array?
 * To add planets, call the planetarySystem class in which the array was made.
 * 
 * 3 - Can you figure out how to add eight other planets of our solar system to the planets array? 
 * A similar method as the given example but with a different name, description, and index.
 * 
 * 4 - What if the planetary system has more than 8 planets?  We need a better data structure to hold our planet objects.
 * If there were more than 8 planets in the planetary system there would be an error and the code wont run.
 * A better data structure is an array list because the number of ibjects it contains is not limited
 * 
 * 5 - The SpaceAdventure constructor is repetitive.  We should refactor.  What does it mean to "refactor" the initializer? 
 * Refactring the constructor means editing the code to improve its efficiency. It also removes repetitive code overall.
 * 
 * 6 - Can you think of a way we can use the array of planets to let the traveler specify the planet he or she wishes to travel to?
 * You can take a response from the user, cycle trhough the planets using a for loop, and check the planet the user entered in.
 * If it is true then the user will visit that planet.
 * 
 * 7 - How can we ask the traveler which planet he or she would like to visit, and then display that planet's description? 
 * We can use responsetoPrompt to get a response from the user, and check using a for loop in the array of stored planets.
 * If the result is true then the planet's description can be seen using planet.description.
 * 
 * 8 - What happens when the traveler types something else besides a valid planet name? 
 * None of the planets would be visited and the code will terminate.
 * 
 * 9 - Why is our program crashing with a runtime error? 
 * Can you think of a way we can improve the code, to handle cases where the array of planets is empty?
 * The code has not been made to recgnize situations where the array of planets is empty
 * This can be fixed by introducting such a situation in the code
 * 
 * 10 - Our codebase has grown, and the SpaceAdventure initializer has a bit of a "code smell." 
 * Although it works ok, can you think of ways we can improve the initializer? 
 * We can clean up the overall code and remove visible repetitions
 */

public class SpaceAdventure
{
    public SpaceAdventure() 
    {
        public Planet(PlanterarySystem);
        {
            this.PlanetarySystem = planet;
        }
        Planet 51 Pegasi b = new Planet("51 Pegasi b", "Like Jupiter but bigger and really hot");
        planets[1] = 51 Pegasi b;
        Planet 70 Virginis = new Planet("70 Virginis", "A cool exoplanet far away");
        planets[2] = 70 Virginis;
        Planet 47 Ursae Majoris = new Planet("47 Ursae Majoris", "A hot and large planet");
        planets[3] = 47 Ursae Majoris;
        Planet Kepler-22b = new Planet("Kepler-22b", "A blue planet similar to earth");
        planets[4] = Kepler-22b;
        Planet Gliese 221 b = new Planet("Gliese 221 b", "A volcanic planet very close to a star");
        planets[5] = Gliese 221 b;
        Planet Eris = new Planet("Eris", "A dwarf planet that is outside the solar system");
        planets[6] = Eris;
        Planet Ceres = new Planet("Ceres", "A very small and cold dwarf planet");
        planets[7] = Ceres;
        Planet Pluto = new Planet("Pluto", "A distant and well known dwarf planet");
        planets[8] = Pluto;
        planetarySystem = new PlanetarySystem("Solar System", planets);
    }
          
    public void start ()
    {
        displayIntroduction();
        greetAdventurer();
        if(!(planetarySystem.planets == null || planetarySystem.planets.length == 0))
        {
            determineDestination();
        }
    }
    
    public static void main()
    {
        SpaceAdventure adventure = new SpaceAdventure();
        adventure.start();
    }
    
    private void displayIntroduction()
    {
        System.out.println("Welcome to the " + planetarySystem.name + "!");
        System.out.println("There are " + planetarySystem.planets.length + "to explore.");
    }
    
    private static String responseToPrompt (String Prompt)
    {
        System.out.println(prompt);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
    
    private void greetAdventurer()
    
    {
        String name = responseToPrompt("What is your name?");
        System.out.println("Hello" + name + "my name is Pawan");
    }
    
    public void determineDestination() 
    {
        boolean madechoice = false;
        String choice = responseToPrompt("We're going on an adventure, should I randomly select a plant to visit? Y or N.");
        if(choice.equals("Y"))
        {
            madeChoice = true;
            Planet planet = planetarySystem.randomPlanet();
            if(planet !=null)
            {
                visit(planet.name);
            }
            else
            {
                System.out.println("Sorry, there are no planets in this system");
            }
        }
        if(choice.equals("N"))
        {
            madechoice = true;
            choosePlanet();
        }
        while(!madechoice)
        {
            String choice2 =resposeToPrompt("Invalid answer, please enter Y or N.");
            if(choice2.equals("Y"))
            {
                visit(planet.name);
            }
            else
            {
                System.out.println("Sorry there are no planets in this system");
            }
            if(choice2.equals("N"))
            {
                madechoice = true;
                choosePlanet();
            }
        }
    }
    
    public void choosePlanet()
    {
        String planetChoice = responseToPrompt("Which planet should we go to?");
        visit(planetChoice);
    }
     
    public void visit(String planetChoice)
    {
        boolean madechoice = false;
        String chooseAgain = "";
        while(!planetChosen)
        {
            for (Planet planet: planetarySystem.planets)
            {
                if(planetChoice.equals(planet.name))
                {
                    System.out.println("Travelling to " + planetChoice + "...");
                    System.out.println("Arrived at " + planet.name + ". " + planet.description + ". ");
                    planetChosen = true;
                
                }
                if (chooseAgain.equals(planet,name))
                {
                    System.out.println("Travelling to " + chooseAgain + "...");
                    System.out.println("Arrived at " + planet.name + ". " + planet.description + ". ");
                    planetChosen = true;
                }
            }
            if(!planetChosen)
            {
                chooseAgain = responseToPrompt("Please type the name of a planet.");
            }
        }
    } 
}